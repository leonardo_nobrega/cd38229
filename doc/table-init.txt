@startuml

title Metrics table initialization

skinparam class {
  backgroundColor<<top-level>> #FFDDA0
  backgroundColor<<clj-protocol>> #E0E0E0
  backgroundColor<<clj-def>> White
}

namespace cenx.plataea.cassandra {
  namespace init {
    class CassandraSchema << (R,white) clj-record >> <<top-level>>
    class init-keyspaces << (F,white) clj-fn >>
    class init-tables << (F,white) clj-fn >>
  }
  namespace vnf {
    class table-name << (D,white) clj-def >>
    class columns << (D,white) clj-def >>
    class create-table << (F,white) clj-fn >>
  }
}

hide empty members

"cenx.plataea.cassandra.init.CassandraSchema" --> \
"cenx.plataea.cassandra.init.init-keyspaces"

"cenx.plataea.cassandra.init.CassandraSchema" --> \
"cenx.plataea.cassandra.init.init-tables"

"cenx.plataea.cassandra.init.init-tables" --> \
"cenx.plataea.cassandra.vnf.create-table"

"cenx.plataea.cassandra.vnf.create-table" --> \
"cenx.plataea.cassandra.vnf.table-name"

"cenx.plataea.cassandra.vnf.create-table" --> \
"cenx.plataea.cassandra.vnf.columns"

@enduml
